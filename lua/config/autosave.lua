-- TODO for later use cases to run and test parallely
local vim = vim

local attach_to_buffer = function(opBufnr, pattern, cmd)
    vim.api.nvim_create_autocmd("BufWritePost", {
        group = vim.api.nvim_create_augroup("AutoSaveGroup", { clear = true }),
        pattern = pattern,
        callback = function()
            local append_data = function(_, data)
                if data then
                    vim.api.nvim_buf_set_lines(opBufnr, -1, -1, false, { "testing ..." })
                    vim.api.nvim_buf_set_lines(opBufnr, -1, -1, false, data)
                end
            end
            vim.api.nvim_buf_set_lines(opBufnr, 0, -1, false, { "main.go output:" })
            vim.fn.jobstart(cmd, {
                stdout_buffered = true,
                on_stdout = append_data,
                on_stderr = append_data,
            })
        end,
    })
end

vim.api.nvim_create_user_command("AutoRun", function()
    print "AutoRun starts now ..."
    local opBufnr = vim.fn.input "Bufnr to run: "
    local pattern = vim.fn.input "Pattern of files: "
    local cmd = vim.split(vim.fn.input "Command to test: ", " ")
    attach_to_buffer(tonumber(opBufnr), pattern, cmd)
end, {})
