-- LSP settings

local mason = require "mason"

-- Provide settings first!
mason.setup {
    ui = {
        icons = {
            package_installed = "✓",
            package_pending = "➜",
            package_uninstalled = "✗",
        },
    },
}

require("mason-lspconfig").setup {
    ensure_installed = { "lua_language_server" },
}
