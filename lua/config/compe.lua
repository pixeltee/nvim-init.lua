local vim = vim
-- nvim-cmp supports additional completion capabilities
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require("cmp_nvim_lsp").update_capabilities(capabilities)
local lspkind = require "lspkind"

local check_backspace = function()
    local col = vim.fn.col "." - 1
    return col == 0 or vim.fn.getline("."):sub(col, col):match "%s"
end

require("luasnip/loaders/from_vscode").lazy_load()
-- nvim-cmp setup
local cmp = require "cmp"
local luasnip = require "luasnip"
cmp.setup {
    completion = {
        completeopt = "menu,menuone,noinsert",
    },
    snippet = {
        expand = function(args)
            luasnip.lsp_expand(args.body)
        end,
    },
    mapping = {
        ["<C-d>"] = cmp.mapping.scroll_docs(-4),
        ["<C-f>"] = cmp.mapping.scroll_docs(4),
        ["<C-Space>"] = cmp.mapping.complete {
            config = {
                sources = {
                    { name = "vsnip" },
                },
            },
        },
        ["<C-e>"] = cmp.mapping.close(),
        ["<c-y>"] = cmp.mapping.confirm {
            behavior = cmp.ConfirmBehavior.Replace,
            select = true,
        },
        ["<Tab>"] = cmp.mapping(function(fallback)
            if cmp.visible() then
                cmp.select_next_item()
            else
                fallback()
            end
        end, {
            "i",
            "s",
        }),
    },
    sources = {
        { name = "gh_issues" },
        { name = "buffer" },
        { name = "path" },
        { name = "luasnip" },
        { name = "nvim_lua" },
        { name = "nvim_lsp" },
    },
    formatting = {
        format = lspkind.cmp_format {
            mode = "symbol",
            with_text = true,
            maxwidth = 100,
            menu = {
                buffer = "[buf]",
                nvim_lsp = "[LSP]",
                nvim_lua = "[api]",
                path = "[path]",
                luasnip = "[snip]",
                gh_issues = "[issues]",
            },
        },
        fields = { "kind", "abbr", "menu" },
    },
    experimental = {
        ghost_text = {
            hl_group = "LspCodeLens",
        },
    },
    view = {
        entries = "native",
    },
    window = { documentation = false },
}
