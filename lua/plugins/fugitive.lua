return {
    "tpope/vim-fugitive",
    lazy = false,
    priority = 1000,
    opts = {},
    dependencies = {
        "tpope/vim-rhubarb",
        "tpope/vim-commentary",
    },
    config = function() end,
}
