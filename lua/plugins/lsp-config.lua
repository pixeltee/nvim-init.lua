local M = {
    {
        "williamboman/mason.nvim",
        enabled = true,
        event = "VeryLazy",
        config = function()
            require("mason").setup()
        end,
    },
    {
        "williamboman/mason-lspconfig.nvim",
        enabled = true,
        config = function()
            require("mason-lspconfig").setup {
                ensure_installed = { "lua_ls", "gopls", "jdtls" },
            }
        end,
    },
    {
        "neovim/nvim-lspconfig",
        enabled = true,
        event = "VeryLazy",
        config = function()
            local lspconfig = require "lspconfig"

            lspconfig.lua_ls.setup {}
            lspconfig.gopls.setup {}

            local runtime_path = vim.split(package.path, ";")
            table.insert(runtime_path, "lua/?.lua")
            table.insert(runtime_path, "lua/?/init.lua")
            local servers = {
                lua_ls = {
                    Lua = {
                        telemetry = { enable = false },
                        runtime = {
                            version = "LuaJIT",
                            path = runtime_path,
                        },
                        diagnostics = {
                            -- Get the language server to recognize the `vim` global
                            globals = { "vim" },
                        },
                        workspace = {
                            checkThirdParty = false,
                            library = {
                                -- Make the server aware of Neovim runtime files
                                vim.fn.expand "$VIMRUNTIME/lua",
                                vim.fn.stdpath "config" .. "/lua",
                            },
                        },
                        completion = {
                            callSnippet = "Replace",
                        },
                    },
                },
            }

            local on_attach = function(client, bufnr)
                -- map buffer local keys once lsp is attached
                MapLSP(bufnr)

                if client.name == "ruff_lsp" then
                    -- Disable this in favor of pyrite's hover
                    client.server_capabilities.hoverProvider = false
                end

                -- local vt = {
                -- 	spacing = 10,
                -- 	severity = { min = vim.diagnostic.severity.INFO },
                -- 	format = function(diagnostic)
                -- 		local severity_letter = string.sub(vim.diagnostic.severity[diagnostic.severity], 1, 1)
                -- 		local msg = diagnostic.message
                -- 		local msg_threshold = 100

                -- 		local num_windows = #vim.api.nvim_tabpage_list_wins(0) / 2
                -- 		if num_windows > 1 then
                -- 			msg_threshold = msg_threshold / num_windows
                -- 		end

                -- 		if string.len(msg) > msg_threshold then
                -- 			msg = string.sub(msg, 1, msg_threshold) .. "..."
                -- 		end

                -- 		return string.format("%s: %s", severity_letter, msg)
                -- 	end,
                -- }
                local vt_basic = {
                    spacing = 10,
                    severity = { min = vim.diagnostic.severity.INFO },
                }
                vim.diagnostic.config { virtual_text = vt_basic }

                -- create buffer local autocommands to show and hide virtual diagnostic text
                -- vim.api.nvim_create_autocmd({ "CursorHold", "CursorHoldI" }, {
                -- 	buffer = bufnr,
                -- 	callback = function()
                -- 		vim.diagnostic.config({ virtual_text = vt })
                -- 	end,
                -- })
                -- vim.api.nvim_create_autocmd({ "CursorMoved", "CursorMovedI" }, {
                -- 	buffer = bufnr,
                -- 	callback = function()
                -- 		vim.diagnostic.config({ virtual_text = false })
                -- 	end,
                -- })
            end

            -- setup lsp diagnostic signs
            vim.fn.sign_define("DiagnosticSignError", { texthl = "DiagnosticSignError", text = "", numhl = "" })
            vim.fn.sign_define("DiagnosticSignWarn", { texthl = "DiagnosticSignWarn", text = "", numhl = "" })
            vim.fn.sign_define("DiagnosticSignHint", { texthl = "DiagnosticSignHint", text = "", numhl = "" })
            vim.fn.sign_define("DiagnosticSignInfo", { texthl = "DiagnosticSignInfo", text = "", numhl = "" })
        end,
    },
}

return M
