return {
    "nvim-treesitter/nvim-treesitter",
    run = ":TSUpdate",
    config = function()
        local status_ok, configs = pcall(require, "nvim-treesitter.configs")
        if not status_ok then
            return
        end

        configs.setup {
            ensure_installed = {
                "vim",
                "java",
                "python",
                "go",
                "lua",
            },
            sync_install = false,
            highlight = {
                enable = true,
                addtional_vim_regex_highlighting = false,
                use_languagetree = true,
            },
            playground = {
                enable = true,
                keybindings = {
                    toggle_query_editor = "o",
                    toggle_hl_groups = "i",
                    toggle_injected_languages = "t",
                    toggle_anonymous_nodes = "a",
                    toggle_language_display = "I",
                    focus_language = "f",
                    unfocus_language = "F",
                    update = "R",
                    goto_node = "<cr>",
                    show_help = "?",
                },
            },
            rainbow = {
                enable = true,
                extended_mode = true,
                colors = {
                    none = "NONE",
                    bg_dark = "#1f2335",
                    bg = "#24283b",
                    bg_highlight = "#292e42",
                    terminal_black = "#414868",
                    fg = "#c0caf5",
                    fg_dark = "#a9b1d6",
                    fg_gutter = "#3b4261",
                    dark3 = "#545c7e",
                    comment = "#565f89",
                    dark5 = "#737aa2",
                    blue0 = "#3d59a1",
                    blue = "#7aa2f7",
                    cyan = "#7dcfff",
                    blue1 = "#2ac3de",
                    blue2 = "#0db9d7",
                    blue5 = "#89ddff",
                    blue6 = "#B4F9F8",
                    blue7 = "#394b70",
                    magenta = "#bb9af7",
                    magenta2 = "#ff007c",
                    purple = "#9d7cd8",
                    orange = "#ff9e64",
                    yellow = "#e0af68",
                    green = "#9ece6a",
                    green1 = "#73daca",
                    green2 = "#41a6b5",
                    teal = "#1abc9c",
                    red = "#f7768e",
                    red1 = "#db4b4b",
                    git = { change = "#6183bb", add = "#449dab", delete = "#914c54", conflict = "#bb7a61" },
                    gitSigns = { add = "#164846", change = "#394b70", delete = "#823c41" },
                },
            },
            incremental_selection = {
                enable = true,
                keymaps = {
                    init_selection = "gnn",
                    node_incremental = "grn",
                    scope_incremental = "grc",
                    node_decremental = "grm",
                },
            },
            indent = {
                enable = true,
                disable = { "yaml" },
            },
            context_commentstring = {
                enable = true,
                enable_autocmd = false,
            },
            textobjects = {
                select = {
                    enable = true,
                    lookahead = true, -- Automatically jump forward to textobj, similar to targets.vim
                    keymaps = {
                        -- You can use the capture groups defined in textobjects.scm
                        ["af"] = "@function.outer",
                        ["if"] = "@function.inner",
                        ["ac"] = "@class.outer",
                        ["ic"] = "@class.inner",
                    },
                },
                move = {
                    enable = true,
                    set_jumps = true, -- whether to set jumps in the jumplist
                    goto_next_start = {
                        ["]m"] = "@function.outer",
                        ["]]"] = "@class.outer",
                    },
                    goto_next_end = {
                        ["]M"] = "@function.outer",
                        ["]["] = "@class.outer",
                    },
                    goto_previous_start = {
                        ["[m"] = "@function.outer",
                        ["[["] = "@class.outer",
                    },
                    goto_previous_end = {
                        ["[M"] = "@function.outer",
                        ["[]"] = "@class.outer",
                    },
                    lsp_interop = {
                        enable = true,
                        border = "none",
                        peek_definition_code = {
                            ["<leader>df"] = "@function.outer",
                            ["<leader>dF"] = "@class.outer",
                        },
                    },
                },
            },
        }
    end,
}
