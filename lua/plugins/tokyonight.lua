return {
    "folke/tokyonight.nvim",
    lazy = false,
    priority = 1000,
    config = function()
        vim.cmd.colorscheme "tokyonight-moon"
        -- set termguicolors
        -- colorscheme tokyonight
    end,
} -- Best color scheme
