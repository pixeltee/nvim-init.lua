return {
    "lukas-reineke/indent-blankline.nvim",
    version = "2.20.8",
    config = function()
        local vim = vim
        --Map blankline
        local status_ok, indent = pcall(require, "indent_blankline")
        if not status_ok then
            return
        end

        indent.setup {
            space_char_blankline = " ",
            show_current_context = true,
            show_current_context_start = true,
        }
        vim.g.indent_blankline_char = "┊"
        vim.g.indent_blankline_filetype_exclude = { "help", "packer" }
        vim.g.indent_blankline_buftype_exclude = { "terminal", "nofile" }
        vim.g.indent_blankline_char_highlight = "LineNr"
        vim.g.indent_blankline_show_trailing_blankline_indent = false
    end,
}
