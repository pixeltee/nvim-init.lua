return {
    "itchyny/lightline.vim",
    config = function()
        local vim = vim
        vim.g.lightline = {
            colorscheme = "tokyonight",
            -- active = { left = { { 'mode', 'paste' }, { 'gitbranch', 'readonly', 'filename', 'modified' } } },
            component_function = { gitbranch = "fugitive#head" },
        }
    end,
}
