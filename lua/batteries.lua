--     vim:tw=78:ts=8:noet:fdm=indent
--     ------------------------------------------------------------------------------
--     use "tpope/vim-surround"
--     use 'ludovicchabant/vim-gutentags' -- Automatic tags management
--     use "audibleblink/hackthebox.vim" -- Theme inspired by Atom
--     use "EdenEast/nightfox.nvim" -- Theme inspired by Atom
--     use "folke/zen-mode.nvim" -- Theme inspired by Atom
-- 	use "neovim/nvim-lspconfig"
-- 	use "jose-elias-alvarez/null-ls.nvim"
-- 	use "mhartington/formatter.nvim"
--     use { "williamboman/mason.nvim", "williamboman/mason-lspconfig.nvim" }
--     use "onsails/lspkind-nvim" -- nice colors for snippets
--     -------SNIPPETS & COMPLETION
--     use "hrsh7th/nvim-cmp" -- Autocompletion plugin
--     use "hrsh7th/cmp-buffer"
--     use "hrsh7th/cmp-path"
--     use "hrsh7th/cmp-nvim-lua"
--     use "hrsh7th/cmp-nvim-lsp"
--     use "saadparwaiz1/cmp_luasnip"
--     use "L3MON4D3/LuaSnip" -- Snippets engine
--     use "rafamadriz/friendly-snippets" -- useful snippets
-- 	use {
-- 		"xuhdev/vim-latex-live-preview",
-- 		filetype = "tex",
-- 	}

local lazypath = vim.fn.stdpath "data" .. "/lazy/lazy.nvim"

if not vim.loop.fs_stat(lazypath) then
    vim.fn.system {
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        "--branch=stable", -- latest stable release
        lazypath,
    }
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup "plugins"
